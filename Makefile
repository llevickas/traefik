default:
	mkcert -cert-file certs/cert.pem -key-file certs/key.pem "localhost"
	(sudo docker rm --force traefik) || echo 'ok'
	sudo docker run \
		--name traefik \
		--restart unless-stopped \
		-p 8080:8080 -p 80:80 -p 443:443 \
		-d \
		-v /var/run/docker.sock:/var/run/docker.sock \
		-v `pwd`/certs/:/etc/certs \
		-v `pwd`/config.yml/:/etc/traefik/config.yml \
		--network traefik \
		traefik:latest \
		--api.insecure=true \
		--entrypoints.web-secure.address=:443 \
		--entrypoints.web.address=:80 \
		--providers.docker \
		--providers.file.filename=/etc/traefik/config.yml
		#--providers.docker.tls.insecureSkipVerify=true \
		#--providers.docker.tls.caOptional=true
		#--providers.docker.tls.cert=/etc/certs/cert.pem \
		#--providers.docker.tls.key=/etc/certs/key.pem
